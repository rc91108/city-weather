package com.baba.cityWeather.utils

import android.util.Log

class Constant {

    interface IntentKey {
        companion object {
            val CITY_NAME = "city_name"
        }
    }

    companion object {
        const val SAMPLE_KEY = "84a332a658c86bf7d458f2adc7e09896"

        fun showLog(tag: String,message:String){
            Log.d(tag,message)
        }
    }

}
