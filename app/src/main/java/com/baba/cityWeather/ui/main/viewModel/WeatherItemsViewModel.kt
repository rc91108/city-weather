package com.baba.cityWeather.ui.main.viewModel

import androidx.databinding.ObservableField

import com.baba.cityWeather.ui.main.model.MainResponseModel

class WeatherItemsViewModel(private val mainModel: MainResponseModel, private val eventViewModelListener: DevViewModelListener) {

    val cityName = ObservableField<String>()
    val tempMin = ObservableField<String>()
    val tempMax = ObservableField<String>()
    val wind = ObservableField<String>()
    val description = ObservableField<String>()
    val dateTime = ObservableField<String>()

    init {
        this.cityName.set(mainModel.name)
        this.tempMin.set(mainModel.main.temp_min.toString())
        this.tempMax.set(mainModel.main.temp_max.toString())
        this.wind.set(mainModel.wind.speed.toString())
        this.description.set(mainModel.weather[0].description)
        this.dateTime.set(mainModel.dt_txt)
    }

    fun onItemClick() {
        eventViewModelListener.onItemClick(mainModel)
    }

    interface DevViewModelListener {
        fun onItemClick(model: MainResponseModel)
    }
}
