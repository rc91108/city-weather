package com.baba.cityWeather.ui.main.module

import com.baba.cityWeather.ui.main.viewModel.HomeActivityViewModel
import dagger.Module
import dagger.Provides

@Module
class HomeActivityModule {

    @Provides
    fun provideDevViewModel() = HomeActivityViewModel()

}