package com.baba.cityWeather.ui.main.activities

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.baba.cityWeather.BR
import com.baba.cityWeather.R
import com.baba.cityWeather.base.BaseActivity
import com.baba.cityWeather.databinding.ActivityForecastBinding
import com.baba.cityWeather.ui.main.interfacess.BaseNavigation
import com.baba.cityWeather.ui.main.adapter.WeathersAdapter
import com.baba.cityWeather.ui.main.viewModel.ForecastActivityViewModel
import com.baba.cityWeather.utils.Constant
import com.baba.cityWeather.utils.Constant.IntentKey.Companion.CITY_NAME
import kotlinx.android.synthetic.main.activity_search_city.*
import javax.inject.Inject
import javax.inject.Provider

class ForecastListActivity : BaseActivity<ActivityForecastBinding, ForecastActivityViewModel>(), BaseNavigation {

    @Inject
    lateinit var forecastActivityViewModel: ForecastActivityViewModel

    @Inject
    internal lateinit var weathersAdapter: WeathersAdapter

    @Inject
    lateinit var mLayoutManager: Provider<RecyclerView.LayoutManager>

    override fun getBindingVariable(): Int = BR.forecastViewModel

    override fun getLayoutId(): Int = R.layout.activity_forecast

    override fun getViewModel(): ForecastActivityViewModel = forecastActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        forecastActivityViewModel.navigator = this
        recyclerViewSetUp()
        subscribeToLiveData()
        val cityName = intent.getStringExtra(CITY_NAME) ?: ""
        forecastActivityViewModel.getForecastData(cityName, Constant.SAMPLE_KEY)
    }


    private fun recyclerViewSetUp() {
        forecastRecycler.apply {
            layoutManager = mLayoutManager.get()
            adapter = weathersAdapter
            addItemDecoration(DividerItemDecoration(this@ForecastListActivity,
                    DividerItemDecoration.VERTICAL))
        }
    }

    private fun subscribeToLiveData() {
        forecastActivityViewModel.forecastRepos.observe(this,
                Observer {
                    forecastActivityViewModel.addWeatherToList(it)
                })
    }
}