package com.baba.cityWeather.ui.main.viewModel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.baba.cityWeather.base.BaseViewModel
import com.baba.cityWeather.retrofit.RetrofitRestClient
import com.baba.cityWeather.ui.main.interfacess.BaseNavigation
import com.baba.cityWeather.ui.main.model.MainResponseModel
import com.baba.cityWeather.ui.main.model.MultipleForecastModel
import com.baba.cityWeather.utils.Constant
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class ForecastActivityViewModel : BaseViewModel<BaseNavigation>() {

    val TAG = "ForecastActivityViewModel"

    val forecastListModel = ObservableArrayList<MainResponseModel>()
    val forecastRepos: MutableLiveData<List<MainResponseModel>> = MutableLiveData()
    val errorText = ObservableField<String>()
    val cityName = ObservableField<String>()


    fun addWeatherToList(openSourceItems: List<MainResponseModel>) {
        forecastListModel.clear()
        forecastListModel.addAll(openSourceItems)
    }


    fun getForecastData(cityList: String, appId: String) {
        setIsLoading(true)

        getCityForecastObservable(cityList, appId).subscribe(
                { apiResponse ->
                    setIsLoading(false)
                    apiResponse.let {
                        errorText.set("")
                        forecastRepos.value = it.list
                        cityName.set(it.city.name)
                    }
                },
                { error ->
                    forecastListModel.clear()
                    errorText.set(error.message)
                    setIsLoading(false)
                    Constant.showLog(TAG, error.message ?: "onUnknownError")
                }
        ).addTo(compositeDisposable)
    }

    private fun getCityForecastObservable(city: String, appId: String): Observable<MultipleForecastModel> {
        return RetrofitRestClient.instance.getFiveDayForecastDetails(city, appId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}