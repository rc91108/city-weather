package com.baba.cityWeather.ui.main.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import com.baba.cityWeather.BR
import com.baba.cityWeather.R
import com.baba.cityWeather.base.BaseActivity
import com.baba.cityWeather.databinding.ActivityHomeBinding
import com.baba.cityWeather.ui.main.viewModel.HomeActivityViewModel
import com.baba.cityWeather.utils.AppPermissions.checkLocationPermission
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject
import android.location.Geocoder
import com.baba.cityWeather.utils.AppPermissions.MY_PERMISSIONS_REQUEST_LOCATION
import com.baba.cityWeather.utils.Constant.IntentKey.Companion.CITY_NAME
import java.util.*


class HomeActivity : BaseActivity<ActivityHomeBinding, HomeActivityViewModel>(), LocationListener {


    @Inject
    lateinit var homeActivityViewModel: HomeActivityViewModel


    private var gpsCallBack = false


    override fun getBindingVariable(): Int = BR.homeViewModel

    override fun getLayoutId(): Int = R.layout.activity_home

    override fun getViewModel(): HomeActivityViewModel = homeActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchCityButton.setOnClickListener {
            startActivity(Intent(this, SearchCityActivity::class.java))
        }
        findCityButton.setOnClickListener {
            gpsPermission()
        }
    }


    private fun gpsPermission() {
        if (checkLocationPermission(this)) {
            gpsCallBack = true
            checkGpsOn()
        }
    }

    private fun checkGpsOn() {
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsEnabled = false
        var networkEnabled = false

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }

        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }

        if (!gpsEnabled && !networkEnabled) {
            // notify user
            gpsCallBack = true
            val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(myIntent)
        } else {
            if (gpsCallBack) {
                gpsCallBack = false
                val cityName = getCurrentLocation()
                val intent = Intent(this, ForecastListActivity::class.java)
                intent.putExtra(CITY_NAME, cityName)
                startActivity(intent)
            }
        }
    }

    // if want to update else remove requestLocationUpdates() and LocationListener
    private val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 10000 // 10000 meters
    private val MIN_TIME_BW_UPDATES = (1000 * 60 * 10).toLong() // 10 minute


    @SuppressLint("MissingPermission")
    private fun getCurrentLocation(): String {
        var location: Location? = null
        var myLatitude: Double = 0.toDouble() // Latitude
        var myLongitude: Double = 0.toDouble() // Longitude
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (isNetworkEnabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this)
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            location?.let {
                myLatitude = it.latitude
                myLongitude = it.longitude
            }
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this)
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        location?.let {
            myLatitude = it.latitude
            myLongitude = it.longitude
        }
        val geoCoder = Geocoder(this, Locale.getDefault())
        val addresses = geoCoder.getFromLocation(myLatitude, myLongitude, 1)
        return addresses[0].locality
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            for ((index, permission) in permissions.withIndex()) {
                val grantResult = grantResults[index]
                if (permission == Manifest.permission.ACCESS_FINE_LOCATION) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        gpsCallBack = true
                        checkGpsOn()
                    }
                }
            }
        }
    }

    override fun onLocationChanged(p0: Location?) {
        // on location change update city
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

}