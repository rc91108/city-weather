package com.baba.cityWeather.ui.main.model


data class WeatherModel(val description: String)

data class WindModel(val speed: Float, val deg: Int)
data class MainModel(val temp_min: Float, val temp_max: Float)