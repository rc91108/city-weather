package com.baba.cityWeather.ui.main.model

data class MultipleForecastModel(val list: List<MainResponseModel>, val city: CityModel)


data class CityModel(val name: String)