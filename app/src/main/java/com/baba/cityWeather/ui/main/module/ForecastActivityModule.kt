package com.baba.cityWeather.ui.main.module

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.baba.cityWeather.ui.main.activities.ForecastListActivity
import com.baba.cityWeather.ui.main.adapter.WeathersAdapter
import com.baba.cityWeather.ui.main.viewModel.ForecastActivityViewModel
import dagger.Module
import dagger.Provides

@Module
class ForecastActivityModule {

    @Provides
    fun provideForecastViewModel() = ForecastActivityViewModel()

    @Provides
    fun provideMediaAdapter(): WeathersAdapter {
        return WeathersAdapter()
    }
    @Provides
    fun provideRecyclerLayoutManager(activity: ForecastListActivity): RecyclerView.LayoutManager {
        return LinearLayoutManager(activity)
    }
}