package com.baba.cityWeather.ui.main.viewModel

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.baba.cityWeather.base.BaseViewModel
import com.baba.cityWeather.retrofit.RetrofitRestClient
import com.baba.cityWeather.ui.main.interfacess.SearchNavigator
import com.baba.cityWeather.ui.main.model.MainResponseModel

import com.baba.cityWeather.utils.Constant
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

open class SearchActivityViewModel : BaseViewModel<SearchNavigator>() {

    val TAG = "SearchActivityViewModel"

    val errorText = ObservableField<String>("Search by city name(comma separated)")
    val forecastListModel = ObservableArrayList<MainResponseModel>()
    val forecastRepos: MutableLiveData<List<MainResponseModel>> = MutableLiveData()

    fun addWeatherToList(openSourceItems: List<MainResponseModel>) {
        forecastListModel.clear()
        forecastListModel.addAll(openSourceItems)
    }

    fun getForecastData(cityList: String, appId: String) {
        setIsLoading(true)

        flatMapCities(cityList).subscribe(
                { apiResponse ->
                    setIsLoading(false)
                    apiResponse.let {
                        errorText.set("")
                        forecastRepos.value = it
                        navigator.developersListSuccess()
                    }
                },
                { error ->
                    forecastListModel.clear()
                    errorText.set(error.message)
                    setIsLoading(false)
                    Log.d(TAG, error.message ?: "onUnknownError")
                }
        ).addTo(compositeDisposable)
    }

    open fun flatMapCities(cities: String): Observable<MutableList<MainResponseModel>> {
        val cityList = cities.split(",").toTypedArray()

        val cityObservables: MutableList<Observable<MainResponseModel>> = mutableListOf()

        for (cityName in cityList) {
            cityObservables.add(getCityObservable(cityName))
        }

        return Observable.zip(cityObservables) {
            val value = mutableListOf<MainResponseModel>()
            for (data in it) {
                value.add(data as MainResponseModel)
            }
            value
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    private fun getCityObservable(city: String): Observable<MainResponseModel> {
        return RetrofitRestClient.instance.getForecastDetailsCity(city, Constant.SAMPLE_KEY).subscribeOn(Schedulers.io())
    }
}
