package com.baba.cityWeather.ui.main.model


data class MainResponseModel(val name: String, val main: MainModel, val wind: WindModel, val weather: List<WeatherModel>, val dt_txt: String?)

