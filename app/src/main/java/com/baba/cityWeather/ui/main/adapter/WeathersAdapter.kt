package com.baba.cityWeather.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.baba.cityWeather.base.BaseViewHolder
import com.baba.cityWeather.databinding.AdapterWeatherBinding
import com.baba.cityWeather.ui.main.model.MainResponseModel
import com.baba.cityWeather.ui.main.viewModel.WeatherItemsViewModel

class WeathersAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    internal var mOpenSourceResponseList: List<MainResponseModel> = mutableListOf()

    fun addData(weatherDataModels: List<MainResponseModel>) {
        mOpenSourceResponseList = weatherDataModels
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemView = AdapterWeatherBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.onBind(position, mOpenSourceResponseList)

    override fun getItemCount(): Int = mOpenSourceResponseList.size


    inner class ViewHolder(private val mBinding: AdapterWeatherBinding) : BaseViewHolder(mBinding.root),
            WeatherItemsViewModel.DevViewModelListener {

        private var articleItemViewModel: WeatherItemsViewModel? = null

        override fun onBind(position: Int, dataObject: Any?) {
            articleItemViewModel = WeatherItemsViewModel(mOpenSourceResponseList[position], this)
            this.mBinding.weatherModel = articleItemViewModel
            this.mBinding.executePendingBindings()
        }

        override fun onItemClick(model: MainResponseModel) {
        }
    }
}
