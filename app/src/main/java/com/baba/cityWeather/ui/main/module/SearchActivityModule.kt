package com.baba.cityWeather.ui.main.module

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.baba.cityWeather.ui.main.activities.SearchCityActivity
import com.baba.cityWeather.ui.main.viewModel.SearchActivityViewModel
import com.baba.cityWeather.ui.main.adapter.WeathersAdapter

import dagger.Module
import dagger.Provides

@Module
class SearchActivityModule {
    @Provides
     fun provideDevViewModel(): SearchActivityViewModel {
        return SearchActivityViewModel()
    }

    @Provides
     fun provideMediaAdapter(): WeathersAdapter {
        return WeathersAdapter()
    }


    @Provides
     fun provideRecyclerLayoutManager(activity: SearchCityActivity): RecyclerView.LayoutManager {
        return LinearLayoutManager(activity)
    }
}
