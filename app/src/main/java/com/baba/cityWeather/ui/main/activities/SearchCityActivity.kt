package com.baba.cityWeather.ui.main.activities

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.baba.cityWeather.BR
import com.baba.cityWeather.R
import com.baba.cityWeather.base.BaseActivity
import com.baba.cityWeather.databinding.ActivitySearchCityBinding

import com.baba.cityWeather.ui.main.interfacess.SearchNavigator
import com.baba.cityWeather.ui.main.adapter.WeathersAdapter
import com.baba.cityWeather.ui.main.viewModel.SearchActivityViewModel
import com.baba.cityWeather.utils.Constant
import kotlinx.android.synthetic.main.activity_search_city.*
import javax.inject.Inject
import javax.inject.Provider


class SearchCityActivity : BaseActivity<ActivitySearchCityBinding, SearchActivityViewModel>(), SearchNavigator {

    val TAG = "SearchCityActivity"

    @Inject
    lateinit var searchActivityViewModel: SearchActivityViewModel

    @Inject
    internal lateinit var weathersAdapter: WeathersAdapter


    @Inject
    lateinit var mLayoutManager: Provider<RecyclerView.LayoutManager>

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_search_city
    }

    override fun getViewModel(): SearchActivityViewModel? {
        return searchActivityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchActivityViewModel.navigator = this
        recyclerViewSetUp()
        subscribeToLiveData()

    }

    private fun recyclerViewSetUp() {
        forecastRecycler.apply {
            layoutManager = mLayoutManager.get()
            adapter = weathersAdapter
            addItemDecoration(DividerItemDecoration(this@SearchCityActivity,
                    DividerItemDecoration.VERTICAL))
        }
    }

    private fun subscribeToLiveData() {
        searchActivityViewModel.forecastRepos.observe(this,
                Observer {
                    searchActivityViewModel.addWeatherToList(it)
                })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView? = searchItem?.actionView as SearchView

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // check string contain min 3 city and max 7
                if (validateCities(query)) {
                    Constant.showLog(TAG, query)
                    searchActivityViewModel.getForecastData(query, Constant.SAMPLE_KEY)

                }
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun developersListSuccess() {
    }

    fun validateCities(cityName: String): Boolean {
        val cityList = cityName.split(",").toTypedArray()
        return when {
            cityList.size < 3 -> {
                Toast.makeText(this, "Min 3 cities required", Toast.LENGTH_SHORT).show()
                false
            }
            cityList.size > 8 -> {
                Toast.makeText(this, "You can search max 7 cities", Toast.LENGTH_SHORT).show()
                false
            }
            else -> true
        }
    }
}
