package com.baba.cityWeather.retrofit

import com.baba.cityWeather.ui.main.model.MainResponseModel
import com.baba.cityWeather.ui.main.model.MultipleForecastModel
import io.reactivex.Observable

import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {


    @GET("data/2.5/weather")
    fun getForecastDetailsCity(@Query("q") cityName: String, @Query("appid") appid: String): Observable<MainResponseModel>


    @GET("data/2.5/forecast")
    fun getFiveDayForecastDetails(@Query("q") cityName: String, @Query("appid") appid: String): Observable<MultipleForecastModel>


}
