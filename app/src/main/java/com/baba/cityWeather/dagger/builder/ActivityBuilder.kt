package com.baba.cityWeather.dagger.builder


import com.baba.cityWeather.ui.main.activities.ForecastListActivity
import com.baba.cityWeather.ui.main.activities.HomeActivity
import com.baba.cityWeather.ui.main.activities.SearchCityActivity
import com.baba.cityWeather.ui.main.module.ForecastActivityModule
import com.baba.cityWeather.ui.main.module.HomeActivityModule
import com.baba.cityWeather.ui.main.module.SearchActivityModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [SearchActivityModule::class])
    abstract fun bindSearchActivity(): SearchCityActivity

    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [ForecastActivityModule::class])
    abstract fun bindForecastActivity(): ForecastListActivity

}