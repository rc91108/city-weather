package com.baba.cityWeather


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.baba.cityWeather.CustomAssertions.Companion.hasItemCount
import com.baba.cityWeather.CustomMatchers.Companion.withItemCount
import com.baba.cityWeather.ui.main.activities.SearchCityActivity
import org.junit.Rule
import org.junit.Test

class SearchCityActivityTest {
  @Rule
  @JvmField
  var activityRule = ActivityTestRule<SearchCityActivity>(SearchCityActivity::class.java)


  @Test
  fun countPrograms() {
    onView(withId(R.id.forecastRecycler))
        .check(matches(withItemCount(0)))
  }

  @Test
  fun countProgramsWithViewAssertion() {
    onView(withId(R.id.forecastRecycler))
        .check(hasItemCount(0))
  }
}
