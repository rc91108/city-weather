package com.baba.cityWeather

import com.baba.cityWeather.ui.main.model.*
import com.baba.cityWeather.ui.main.viewModel.SearchActivityViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import org.junit.Test
import org.mockito.Mockito.`when`

class ForecastUseCaseTest {

    @Test
    fun `trending , useCase execute`() {
        val mock = mock<SearchActivityViewModel>()
        val mainResponseModel = ArrayList<MainResponseModel>()
        mainResponseModel.apply {
            add(MainResponseModel("Delhi", MainModel(12.9f, 15.2f), WindModel(20f, 0), listOf(WeatherModel("cold")),""))
            add(MainResponseModel("Mumbai", MainModel(20.7f, 25.3f), WindModel(50f, 0), listOf(WeatherModel("mm")),""))
            add(MainResponseModel("Pune", MainModel(18.3f, 25.5f), WindModel(55f, 0), listOf(WeatherModel(" wind")),""))
        }


        `when`(mock.flatMapCities("delhi,mumbai,pune")).thenReturn(Observable.just(mainResponseModel))

        mock.flatMapCities("delhi,mumbai,pune")
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(mainResponseModel)

        verify(mock).flatMapCities("delhi,mumbai,pune")
    }
}