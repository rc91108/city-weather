package com.baba.cityWeather

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.baba.cityWeather.retrofit.ApiService
import com.baba.cityWeather.ui.main.model.*
import com.baba.cityWeather.ui.main.viewModel.SearchActivityViewModel
import com.baba.cityWeather.utils.Constant
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class ForecastRetroUseCaseTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var searchActivityViewModel: SearchActivityViewModel
    private var apiService = providesRetrofit()


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        searchActivityViewModel = SearchActivityViewModel()
    }


    @Test
    fun `live data, trending success`() {
        val value = listOf(MainResponseModel("Delhi", MainModel(12.0f, 15.0f), WindModel(40f, 0), listOf(WeatherModel("dec")),null))
        searchActivityViewModel.forecastRepos.postValue(value)
        assertEquals(value, (searchActivityViewModel.forecastRepos.value))
    }

    @Test
    fun `live data, trending article error`() {
        val value = Throwable("error")
        val mock = mock<SearchActivityViewModel>()
        Mockito.`when`(mock.flatMapCities("")).thenReturn(Observable.error(value))
    }

    @Test
    fun `check, remote data values`() {
        val mainResponseModel: MutableList<MainResponseModel> = mutableListOf()
        mainResponseModel.apply {
            add(MainResponseModel("Delhi", MainModel(12.0f, 15.0f), WindModel(40f, 0), listOf(WeatherModel("dec")),""))
            add(MainResponseModel("Mumbai", MainModel(12.0f, 15.0f), WindModel(40f, 0), listOf(WeatherModel("cold")),""))
            add(MainResponseModel("Pune", MainModel(12.0f, 15.0f), WindModel(40f, 0), listOf(WeatherModel("min wind")),null))
        }

        val searchActivityViewModel: SearchActivityViewModel = mock()
        given { searchActivityViewModel.flatMapCities("") }.willReturn(Observable.just(mainResponseModel))

        searchActivityViewModel.flatMapCities("")
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(mainResponseModel)

        verify(searchActivityViewModel).flatMapCities("")
    }


    @Test
    fun `network request`() {
        val result = apiService.getForecastDetailsCity("delhi", Constant.SAMPLE_KEY)
                .map {
                    "name: ${it.name}, min temp: ${it.main.temp_min}, max temp: ${it.main.temp_max}," +
                            "weather description: ${it.weather[0].description} , wind speed : ${it.wind.speed}"
                }.blockingFirst()

        print(result)
    }


    private fun providesRetrofit(): ApiService {
        return Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .client(OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService::class.java)
    }
}